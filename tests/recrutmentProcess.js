module.exports = {
    'As a user, I would like to apply for a given position': (browser) => {
        const {jobOffersPage, commonPage, applicationFormPage} = getPageObjects(browser)

        const POSITION = {
            category: 'qa',
            level: 'regular'
        }

        const personalData = {
            firstName: 'Joanna',
            lastName: 'Okrajni',
            email: 'jok@gmail.com',
            phone: '123123123',
            dateAvaiableField: '01-05-2020',
            salary: '3000',
            hdyfuField:'From Friends',
            personalDataConfirmationField: 'yes'
        }

        const APPLICATION_MESSAGES = {
            applicationFailure: 'Whoops... No worries. Please fix any missing or incorrect information and try again.',
            }

        jobOffersPage
            .navigate()
            .waitForElementVisible('@jobOffers')

        commonPage.acceptCookies()
        jobOffersPage.findPosition(POSITION.category, POSITION.level)
        commonPage.applyPosition()

        applicationFormPage
            .applyPosition()
            .fillInForms(personalData)
            .submitApplication()
            .assert.containsText('@warningMessage', APPLICATION_MESSAGES.applicationFailure)
    }
}

function getPageObjects(browser) {
    const jobOffersPage = browser.page.jobOffers()
    const commonPage = browser.page.common()
    const applicationFormPage = browser.page.applicationForm()

    return {
        jobOffersPage,
        commonPage,
        applicationFormPage
    }
}
