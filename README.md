# miquido

test app for Miquido recruitment flow - Nightwatch.js

Setup & run
-----

    git clone https://gitlab.com/okrajni/miquido.git
    cd miquido
    npm install
    npm test



Scenario: As a user, I would like to apply for a given position
  
Steps:
*   I open https://careers.miquido.com/job-offers/ page
*   I find a position for QA
*   I open QA position
*   I apply on an opened QA position
*   I fill in an application form (Resume field leave not filled in)
*   I submit application


Acceptance criteria: 
  
*  "Whoops... No worries. Please fix any missing or incorrect information and try again." appears.