module.exports = {
    url: 'https://careers.miquido.com/job-offers/',
    elements: {
        header: '#header',
        jobOffers: `div .job h4`
    },
    commands: [
        {
            findPosition,
        }
    ]
}

function findPosition(category, level){
    let jobOffer = {
        regular: `#${category} .levels > .active:first-child a`,
        senior: `#${category} .levels > .active:last-child a`
    }
    
    this
        .waitForElementPresent(jobOffer[level])
        .click(jobOffer[level])

    return this
}
