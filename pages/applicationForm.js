module.exports = {
    elements: {
        applyButton: 'div.js-jobs-sidebar button',
        
        formHeader: 'h3.applyFormHeader',
        firstNameField: '#firstName',
        lastNameField: '#lastName',
        emailField: '#email',
        phoneField: '#phone',
        dateAvaiableField: '#dateAvailable',
        desiredPayField: '#salary',
        hdyfuField: 'fieldset .fieldRow:nth-child(1) textarea',
        personalDataConfirmationField: 'fieldset .fieldRow:nth-child(2) textarea',

        submitApplication: 'button.SiteFooter__action',
        
        warningMessage: 'span.fab-Slidedown__text--1FOAO'
    },
    commands: [
        {
            applyPosition,
            fillInForms,
            submitApplication
        }
    ]
}


function applyPosition(){
    this
        .waitForElementVisible('@applyButton')
        .click(`@applyButton`)

    return this
}

function fillInForms(personalData){
    this
        .waitForElementPresent('@formHeader')
        .setValue('@firstNameField', personalData.firstName)
        .setValue('@lastNameField', personalData.lastName)
        .setValue('@emailField', personalData.email)
        .setValue('@phoneField', personalData.phone)
        .setValue('@dateAvaiableField', personalData.dateAvaiableField)
        .setValue('@desiredPayField', personalData.salary)
        .setValue('@hdyfuField', personalData.hdyfuField)
        .setValue('@personalDataConfirmationField', personalData.personalDataConfirmationField)

    return this
}

function submitApplication(){
    this
        .waitForElementVisible('@submitApplication')
        .click(`@submitApplication`)

    return this
}
