module.exports = {
    elements: {
        //cookies policy
        cookieNotification: '.cookie-notification',
        cookieAgreeButton: '#cookie-agree',
        cookieReadMoreButton: '#cookie-disagree',
        cookieReadMoreButton: 'a.cookie-close',

        //position pages elements
        applyButton: '#job-levels a',
    },
    commands: [
        {
            acceptCookies,
            applyPosition,
        }
    ]
}

function acceptCookies(){
    this
        .waitForElementPresent('@cookieNotification')
        .click(`@cookieAgreeButton`)

    return this
}

function applyPosition(){
    this
        .waitForElementPresent('@applyButton')
        .getAttribute('@applyButton', 'href', function(result) {
            this.url(result.value)
        })

    return this
}
